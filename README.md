# Ividence Newsletter source client
This project shows how to connect to Google Cloud Storage, using a service account key file, and how to download an HTML file and its metadata.
___
Please ask your account manager for:
- `sa.json` the credentials file,
- `prefix` the object prefix, needed to retreive the file.

## Prerequisites
NodeJS and NPM  

## Preparation
- Move sa.json (provided by ividence) file in this project folder,
- Edit [index.js](./index.js), replace the value of the `prefix` variable with the value ividence gave you.
- Run the command:
```shell
npm install && node index.js
```


## Client libraries
This project use NodeJS and the Cloud Storage NodeJS client library, but feel free to use any language you want. You'll find a list of libraries here:
https://cloud.google.com/storage/docs/reference/libraries

