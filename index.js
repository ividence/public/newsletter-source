const {Storage} = require('@google-cloud/storage');
const moment = require('moment');

const bucket = `ivi-owned-media`;

// Set the ServiceAccount filename here, this file is provided by Ividence.
const storage = new Storage({keyFilename: "sa.json"});

// Replace by the PREFIX provided by Ividence here.
const prefix = 'publisher/newsletter';

async function download(source, destination) {
    await storage
        .bucket(bucket)
        .file(source)
        .download({destination});
}

async function getMetadata(file) {
    const [{contentType, metadata: {subject}}] = await storage.bucket(bucket).file(file).getMetadata();
    return {contentType, subject};
}

const today = moment().format('YYYYMMDD');
const newsletterPath = `${prefix}/newsletter-${today}.html`;
const dest = `${today}.html`

download(newsletterPath, dest)
    .catch(console.error)
    .then(_ => console.log(`${dest} downloaded!`));

getMetadata(newsletterPath)
    .catch(console.error)
    .then(({contentType, subject}) => console.log(`Subject: ${subject}\nContent-Type: ${contentType}\nFile: ${dest}\n====`));
